#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>
 
// Data wire is plugged into pin 2 on the Arduino
#define ONE_WIRE_BUS 2
 
// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

byte mac[] = {0x90, 0xa2, 0xda, 0x00, 0x01, 0x4a};
IPAddress ip(192, 168, 1, 40);
IPAddress server(192, 168, 1, 53);
EthernetClient ethClient;
PubSubClient client(ethClient);
String temperatureStr;
char temp[50];

void reconnect() 
{
  // Loop until we're reconnected
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("arduinoClient")) 
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic","hello world");
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
 
void setup(void)
{
  // start serial port
  Serial.begin(9600);
  Serial.println("read_temp");

  client.setServer(server, 1883);
  Ethernet.begin(mac, ip);
  delay(1500);

  // Start up the library
  sensors.begin();
}
 
 
void loop(void)
{
  sensors.requestTemperatures(); // Send the command to get temperatures
  float temperature = sensors.getTempFByIndex(0);
  temperatureStr = String(temperature);
  temperatureStr.toCharArray(temp, temperatureStr.length() + 1);  
  
  if (!client.connected())
  {
    reconnect();
  }

  client.publish("closetTemperature", temp);
  delay(300000);
}
